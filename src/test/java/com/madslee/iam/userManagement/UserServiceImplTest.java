package com.madslee.iam.userManagement;

import com.madslee.iam.userManagement.exceptions.UserException;
import com.madslee.iam.userManagement.entities.User;
import com.madslee.iam.userManagement.entities.UserEntity;
import com.madslee.iam.userManagement.repositories.UserEntityRepo;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;
import java.util.UUID;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;


@RunWith(SpringRunner.class)
@DataJpaTest
@Transactional
public class UserServiceImplTest {

    @Autowired
    private UserEntityRepo repo;

    private UserEntityRepo repoMock = mock(UserEntityRepo.class);

    private UserServiceImpl service;

    @Before
    public void setup() {
        service = new UserServiceImpl(repo);
    }

    @Test
    public void newUserSavedHasConfirmationKey() {
        var user = getUser();
        User savedUser = service.saveNewUser(user);

        Long userId = savedUser.getId();
        UserEntity savedUserFromDatabase = repo.getOne(userId);
        assertNotNull(savedUserFromDatabase.getConfirmationKeyEntity());
    }

    @Test
    public void confirmedUserHasNoConfirmationKey() {
        var user = getUser();
        user = service.saveNewUser(user);

        String token = user.getConfirmationKeyEntity().getKey();
        Long userId = user.getId();
        service.confirmUserAccount(userId, token);

        user = repo.getOne(userId);
        assertNull(user.getConfirmationKeyEntity());
        assertTrue(user.isEnabled());
    }

    @Test(expected = UserException.class)
    public void confirmNonExistingUser() {
        service.confirmUserAccount(10202356514L, "dsds");
    }

    @Test(expected = UserException.class)
    public void confirmUserWrongToken() {
        var user = getUser();
        user = service.saveNewUser(user);
        Long userId = user.getId();
        service.confirmUserAccount(userId, getRandomToken());
    }

    @Test(expected = UserException.class)
    public void saveNewUserInvalidEmail() {
        service.saveNewUser(new UserEntity("Jo", "jamesgmail", "password"));
    }

    @Test
    public void isActiveUserAccountAndCorrectCredentialsUserNotConfirmed() {
        String email = "john@gmail.com";
        String password = "password";

        UserEntity user = new UserEntity("John", email, password);
        user.setEnabled(false);
        when(repoMock.findByEmail(any())).thenReturn(Optional.of(user));

        assertFalse(service.isActiveUserAccountAndCorrectCredentials(email, password));
    }

    @Test
    public void isActiveUserAccountAndCorrectCredentialsWrongPassword() {
        String email = "john@gmail.com";
        String password = "password";

        UserEntity user = new UserEntity("John", email, password);
        user.setEnabled(true);
        when(repoMock.findByEmail(any())).thenReturn(Optional.of(user));

        assertFalse(service.isActiveUserAccountAndCorrectCredentials(email, password + "wrong"));
    }

    @Test
    public void isActiveUserAccountAndCorrectCredentialNoUserFound() {
        when(repoMock.findByEmail(any())).thenReturn(Optional.empty());
        assertFalse(service.isActiveUserAccountAndCorrectCredentials("email", "password"));
    }

    @Test
    public void passwordIsEncrypted() {
        UserEntity user = getUser();
        String clearTextPassword = user.getPassword();
        service.saveNewUser(user);
        assertNotEquals(user.getPassword(), clearTextPassword);
    }

    private UserEntity getUser() {
        return new UserEntity("John", "john@mail.com", "password");
    }

    private String getRandomToken() {
        return UUID.randomUUID().toString();
    }
}