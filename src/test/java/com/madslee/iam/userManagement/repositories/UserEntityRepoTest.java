package com.madslee.iam.userManagement.repositories;

import com.madslee.iam.userManagement.entities.UserEntity;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.TransactionSystemException;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;


@RunWith(SpringRunner.class)
@DataJpaTest
@Transactional(propagation = Propagation.NOT_SUPPORTED)
public class UserEntityRepoTest {

    @Autowired
    private UserEntityRepo repo;

    @Test(expected = TransactionSystemException.class)
    public void saveUserWithInvalidEmail() {
        var user = new UserEntity("John", "invalid", "password");
        repo.save(user);
    }

    @Test(expected = TransactionSystemException.class)
    public void saveUserWithEmptyUsername() {
        var user = new UserEntity("", "invalid", "password");
        repo.save(user);
    }

    @Test(expected = TransactionSystemException.class)
    public void saveUserWithEmptyPassword() {
        var user = new UserEntity("John", "invalid", "");
        repo.save(user);
    }

    @Test(expected = DataIntegrityViolationException.class)
    public void saveDuplicateEmails() {
        var user = new UserEntity("John", "same@email.com", "password");
        repo.save(user);
        var user2 = new UserEntity("Mike", "same@email.com", "password");
        repo.save(user2);
    }
}