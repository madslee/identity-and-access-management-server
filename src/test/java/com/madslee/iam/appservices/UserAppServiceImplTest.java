package com.madslee.iam.appservices;

import com.madslee.iam.userManagement.entities.User;
import com.madslee.iam.userManagement.entities.UserEntity;
import com.madslee.iam.userManagement.repositories.UserEntityRepo;
import com.madslee.iam.userManagement.exceptions.UserException;
import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest
public class UserAppServiceImplTest {

    @Autowired
    private UserEntityRepo userEntityRepo;

    @Autowired
    private UserAppService userAppService;

    @After
    public void tearDown() {
        userEntityRepo.deleteAll();
    }

    @Test
    public void saveNewUser() {
        String email = "john@gmail.com";

        userAppService.saveNewUser(new UserEntity("John", email, "password"));
        assertTrue(userEntityRepo.findByEmail(email).isPresent());

        UserEntity user = userEntityRepo.findByEmail(email).get();
        assertFalse(user.isEnabled());
        assertNotNull(user.getConfirmationKeyEntity());
    }

    @Test(expected = UserException.class)
    public void saveNewUserWithEmptyPassword() {
        userAppService.saveNewUser(new UserEntity("John", "john@email.com", ""));
    }

    @Test(expected = UserException.class)
    public void saveNewUserWithNullPassword() {
        userAppService.saveNewUser(new UserEntity("John", "john@email.com", null));
    }

    @Test(expected = UserException.class)
    public void saveNewUserDuplicateEmail() {
        String email = "john@gmail.com";

        userAppService.saveNewUser(new UserEntity("John", email, "password"));
        userAppService.saveNewUser(new UserEntity("John", email, "password"));
    }

    @Test
    public void saveNewUserConfirmAndLoginAndGetUserByJwt() {
        String email = "john@gmail.com";
        String password = "password";

        userAppService.saveNewUser(new UserEntity("John", email, "password"));
        assertTrue(userEntityRepo.findByEmail(email).isPresent());

        UserEntity user = userEntityRepo.findByEmail(email).get();

        userAppService.confirmAccount(user.getId(), user.getConfirmationKeyEntity().getKey());
        String jwt = userAppService.login(email, password);

        assertTrue(!jwt.isEmpty() && jwt.length() > 10);

        User userByJwt = userAppService.getUserByJwt(jwt);
        assertEquals(email, userByJwt.getEmail());
        assertEquals(user.getId(), userByJwt.getId());
    }
}