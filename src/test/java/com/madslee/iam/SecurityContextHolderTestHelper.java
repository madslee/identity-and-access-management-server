package com.madslee.iam;

import com.madslee.iam.security.UserPrinciple;
import com.madslee.iam.userManagement.entities.UserEntity;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;

public class SecurityContextHolderTestHelper {

    public static void setSecurityContextHolderUser(UserEntity user) {
        UserDetails userDetails = UserPrinciple.build(user);
        UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(userDetails, null, userDetails.getAuthorities());
        SecurityContextHolder.getContext().setAuthentication(authentication);
    }
}
