package com.madslee.iam.security.jwt;

import com.madslee.iam.userManagement.entities.User;
import com.madslee.iam.userManagement.entities.UserEntity;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;
import static org.springframework.test.util.ReflectionTestUtils.setField;

public class JwtProviderImplTest {

    private String secret = "SECRETCODETOGENERATEJWTMUSTBEOFSUFFICIENTLENGTH";
    private int expiration = 3600;

    private JwtProviderImpl jwtProvider;

    @Before
    public void setup() {
        jwtProvider = new JwtProviderImpl();
        setField(jwtProvider, "jwtSecret", secret);
        setField(jwtProvider, "jwtExpiration", expiration);
    }

    @Test
    public void generateJwt() {
        User user = getUser();

        String jwt = jwtProvider.getJwt(user);
        assertNotNull(jwt);
        assertTrue(jwt.length() > 10);

        String email = jwtProvider.getEmailFromJwt(jwt);
        assertEquals("Email should be derived from JWT", user.getEmail(), email);
    }

    private User getUser() {
        return new UserEntity("John", "john@gmail.com", "password");
    }


}