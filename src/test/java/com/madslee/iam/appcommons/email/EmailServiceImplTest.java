package com.madslee.iam.appcommons.email;

import com.madslee.iam.userManagement.entities.UserEntity;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class EmailServiceImplTest {

    @Autowired
    private EmailServiceImpl emailService;

    @Value("${mail.test.recipient}")
    private String recipient;

    @Test
    public void sendMail() {
        emailService.sendEmail(recipient, "Hei", "Body");
    }

    @Test
    public void sendConfirmationMail() { emailService.sendConfirmationEmail(new UserEntity("John", recipient, "Password"), "Token"); }

}