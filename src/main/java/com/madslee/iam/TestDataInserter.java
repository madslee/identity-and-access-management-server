package com.madslee.iam;

import com.madslee.iam.userManagement.UserService;
import com.madslee.iam.userManagement.entities.UserEntity;
import com.madslee.iam.userManagement.repositories.UserEntityRepo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

@Component
public class TestDataInserter {

    @Autowired
    private UserService userService;

    @Autowired
    private UserEntityRepo userEntityRepo;

    @Value("${context.live.test}")
    private boolean liveTest;

    private final Logger logger = LoggerFactory.getLogger(TestDataInserter.class);

    @EventListener
    public void onApplicationEvent(ContextRefreshedEvent event) {
        if (liveTest) {
            logger.info("property context.live.test is true, inserting test data to database");
            insertUser();
        }
    }

    private void insertUser() {
        // Use service to make sure password encrypted as excepted
        UserEntity user = new UserEntity("John", "john@mail.com", "password");
        user = userService.saveNewUser(user);

        // Bypass enabling logic in service
        user.setEnabled(true);
        userEntityRepo.save(user);
    }
}
