package com.madslee.iam.userManagement;

import com.madslee.iam.userManagement.exceptions.UserManagementExceptionCause;
import com.madslee.iam.userManagement.exceptions.UserException;
import com.madslee.iam.userManagement.entities.ConfirmationKeyEntity;
import com.madslee.iam.userManagement.entities.UserEntity;
import com.madslee.iam.userManagement.repositories.UserEntityRepo;
import org.apache.commons.validator.routines.EmailValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.time.ZonedDateTime;
import java.util.Optional;
import java.util.UUID;

@Service
public class UserServiceImpl implements UserService {

    private UserEntityRepo userEntityRepo;

    private BCryptPasswordEncoder passwordEncoder;

    @Autowired
    public UserServiceImpl(UserEntityRepo userEntityRepo) {
        this.userEntityRepo = userEntityRepo;
        passwordEncoder = new BCryptPasswordEncoder();
    }

    @Override
    public UserEntity saveNewUser(UserEntity user) {
        validateUserEntity(user, true);
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        user.setConfirmationKeyEntity(generateConfirmationKey());
        return userEntityRepo.save(user);
    }

    @Override
    public void confirmUserAccount(Long userId, String token) {
        UserEntity user = getUser(userId);
        if (user.isEnabled()) {
            throw new UserException(UserManagementExceptionCause.USER_ACCOUNT_ALREADY_CONFIRMED);
        }

        ConfirmationKeyEntity confirmationKeyEntity = user.getConfirmationKeyEntity();
        if (!confirmationKeyEntity.getKey().equals(token)) {
            throw new UserException(UserManagementExceptionCause.CONFIRMATION_TOKEN_WRONG);
        }

        user.setEnabled(true);
        user.setConfirmationKeyEntity(null);
        userEntityRepo.save(user);
    }

    @Override
    public boolean isActiveUserAccountAndCorrectCredentials(String email, String password) {
        Optional<UserEntity> userCand = userEntityRepo.findByEmail(email);
        if (userCand.isEmpty()) return false;

        UserEntity user = userCand.get();
        if (!user.isEnabled()) return false;
        return passwordEncoder.matches(password, user.getPassword());
    }

    @SuppressWarnings("OptionalGetWithoutIsPresent")
    @Override
    public UserEntity getUserByEmailUnsafe(String email) {
        return userEntityRepo.findByEmail(email).get();
    }

    private UserEntity getUser(Long userId) {
        Optional<UserEntity> userCandidate = userEntityRepo.findById(userId);
        if (userCandidate.isEmpty()) {
            throw new UserException(UserManagementExceptionCause.INVALID_USER_ID);
        }
        return userCandidate.get();
    }

    private ConfirmationKeyEntity generateConfirmationKey() {
        return new ConfirmationKeyEntity(UUID.randomUUID().toString(), ZonedDateTime.now());
    }

    private void validateUserEntity(UserEntity user, boolean newUser) {
        if (newUser && userEntityRepo.findByEmail(user.getEmail()).isPresent()) {
           throw new UserException(UserManagementExceptionCause.MAIL_ALREADY_IN_USE);
        }

        if (user.getName() == null || user.getName().isEmpty()) {
            throw new UserException(UserManagementExceptionCause.USER_INVALID_NAME);
        }

        if (!isValidEmail(user.getEmail())) {
            throw new UserException(UserManagementExceptionCause.INVALID_EMAIL);
        }

        if (user.getPassword() == null || user.getPassword().length() < user.getMinimumPasswordLength()) {
            throw new UserException(UserManagementExceptionCause.INVALID_PASSWORD);
        }
    }

    private boolean isValidEmail(String email) {
        return EmailValidator.getInstance(true).isValid(email);
    }
}
