package com.madslee.iam.userManagement.exceptions;

import static com.madslee.iam.AppStrings.getString;

public enum UserManagementExceptionCause {

    MAIL_ALREADY_IN_USE,
    USER_INVALID_NAME,
    INVALID_EMAIL,
    INVALID_PASSWORD,
    INVALID_USER_ID,
    USER_ACCOUNT_ALREADY_CONFIRMED,
    CONFIRMATION_TOKEN_WRONG,
    JWT_NOT_ACCEPTED,
    USER_AUTHENTICATION_FAILED,
    LOG_ENTRY_MISSING_START,
    LOG_ENTRY_MISSING_DESCRIPTION,
    LOG_ENTRY_MISSING_USER;

    private final String ERROR_CODE_PREFIX = "USER_ERROR_";

    public String getErrorCode() {
        for (int i = 0; i < values().length; i++) {
            if (this == values()[i]) {
                return ERROR_CODE_PREFIX + i + 1;
            }
        }
        return "";
    }

    public String getCause() {
        if (this == MAIL_ALREADY_IN_USE) return getString("mail.already.in.use");
        if (this == USER_INVALID_NAME) return getString("user.invalid.name");
        if (this == INVALID_EMAIL) return getString("invalid.email");
        if (this == INVALID_PASSWORD) return getString("invalid.password");
        if (this == INVALID_USER_ID) return getString("invalid.user.id");
        if (this == USER_ACCOUNT_ALREADY_CONFIRMED) return getString("user.account.already.confirmed");
        if (this == CONFIRMATION_TOKEN_WRONG) return getString("confirmation.token.wrong");
        if (this == JWT_NOT_ACCEPTED) return getString("jwt.not.accepted");
        if (this == USER_AUTHENTICATION_FAILED) return getString("user.authentication.failed");
        if (this == LOG_ENTRY_MISSING_START) return getString("timelogentry.missing.start");
        if (this == LOG_ENTRY_MISSING_DESCRIPTION) return getString("timelogentry.missing.description");
        if (this == LOG_ENTRY_MISSING_USER) return getString("timelogentry.missing.user");
        return "";
    }
}
