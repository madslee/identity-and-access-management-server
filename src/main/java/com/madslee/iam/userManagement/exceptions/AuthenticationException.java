package com.madslee.iam.userManagement.exceptions;

import com.madslee.iam.appcommons.exceptions.AppException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class AuthenticationException extends AppException {

    public AuthenticationException(UserManagementExceptionCause cause) {
        super(cause.getCause(), cause.getErrorCode());
    }
}
