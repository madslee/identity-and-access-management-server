package com.madslee.iam.userManagement;

import com.madslee.iam.userManagement.entities.UserEntity;

public interface UserService {

    UserEntity saveNewUser(UserEntity user);

    void confirmUserAccount(Long userId, String token);

    boolean isActiveUserAccountAndCorrectCredentials(String email, String password);

    UserEntity getUserByEmailUnsafe(String email);
}
