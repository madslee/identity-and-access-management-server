package com.madslee.iam.userManagement.repositories;

import com.madslee.iam.userManagement.entities.UserEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserEntityRepo extends JpaRepository<UserEntity, Long> {

    @Query
    Optional<UserEntity> findByEmail(String email);

}
