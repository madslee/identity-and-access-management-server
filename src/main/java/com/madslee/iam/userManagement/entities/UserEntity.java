package com.madslee.iam.userManagement.entities;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.Size;

@Entity
@Table(name = "app_user")
public class UserEntity implements User {

    @Id
    @GeneratedValue
    private Long id;

    @Column(nullable = false)
    @Size(min = 2)
    private String name;

    @Column(nullable = false, unique = true)
    @Email
    private String email;

    @Column(nullable = false)
    @Size(min = 8)
    private String password;

    private boolean enabled;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "confirmation_key", referencedColumnName = "id")
    private ConfirmationKeyEntity confirmationKeyEntity;

    public UserEntity() {
    }

    public UserEntity(String name, @Email String email, String password) {
        this.name = name;
        this.email = email;
        this.password = password;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public ConfirmationKeyEntity getConfirmationKeyEntity() {
        return confirmationKeyEntity;
    }

    public void setConfirmationKeyEntity(ConfirmationKeyEntity confirmationKeyEntity) {
        this.confirmationKeyEntity = confirmationKeyEntity;
    }

    public int getMinimumPasswordLength() {
        return 8;
    }
}
