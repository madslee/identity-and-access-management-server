package com.madslee.iam.userManagement.entities;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@JsonSerialize(as=User.class)
public interface User {

    Long getId();
    String getName();
    String getEmail();
    boolean isEnabled();
}
