package com.madslee.iam.userManagement.entities;

import javax.persistence.*;
import java.time.ZonedDateTime;

@Entity
@Table(name = "confirmation_key")
public class ConfirmationKeyEntity {

    @Id
    @GeneratedValue
    private Long id;

    @Column(nullable = false)
    private String key;

    @Column(nullable = false)
    private ZonedDateTime created;

    public ConfirmationKeyEntity() {
    }

    public ConfirmationKeyEntity(String key, ZonedDateTime created) {
        this.key = key;
        this.created = created;
    }

    public Long getId() {
        return id;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public ZonedDateTime getCreated() {
        return created;
    }

    public void setCreated(ZonedDateTime created) {
        this.created = created;
    }
}
