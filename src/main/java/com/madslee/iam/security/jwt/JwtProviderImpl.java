package com.madslee.iam.security.jwt;

import com.madslee.iam.userManagement.entities.User;
import com.madslee.iam.appcommons.email.EmailServiceImpl;
import com.madslee.iam.userManagement.exceptions.AuthenticationException;
import com.madslee.iam.userManagement.exceptions.UserManagementExceptionCause;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.Date;


@Component
public class JwtProviderImpl implements JwtProvider {

    private final Logger logger = LoggerFactory.getLogger(EmailServiceImpl.class);

    @Value("${iam.jwt.secret}")
    private String jwtSecret;
    @Value("${iam.jwt.expiration.ms}")
    private int jwtExpiration;

    @Override
    public String getEmailFromJwt(String jwt) {
        if (!validateJwtToken(jwt)) {
            throw new AuthenticationException(UserManagementExceptionCause.JWT_NOT_ACCEPTED);
        }
        return Jwts.parser()
                .setSigningKey(jwtSecret)
                .parseClaimsJws(jwt)
                .getBody().getSubject();
    }

    @Override
    public String getJwt(User user) {
        return Jwts.builder()
                .setSubject(user.getEmail())
                .setIssuedAt(new Date())
                .setExpiration(new Date((new Date()).getTime() + jwtExpiration))
                .signWith(SignatureAlgorithm.HS256, jwtSecret)
                .compact();
    }

    public boolean validateJwtToken(String authToken) {
        try {
            Jwts.parser().setSigningKey(jwtSecret).parseClaimsJws(authToken);
            return true;
        } catch (Exception e) {
            logger.info("Invalid JWT: " + authToken + ", cause: " + e.getMessage());
        }
        return false;
    }
}
