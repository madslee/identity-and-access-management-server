package com.madslee.iam.security.jwt;


import com.madslee.iam.userManagement.entities.User;

public interface JwtProvider {

    String getEmailFromJwt(String jwt);
    String getJwt(User user);
    boolean validateJwtToken(String jwt);
}
