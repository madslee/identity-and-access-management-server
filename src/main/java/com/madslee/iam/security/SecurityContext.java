package com.madslee.iam.security;

import com.madslee.iam.userManagement.entities.UserEntity;
import org.springframework.security.core.context.SecurityContextHolder;

public class SecurityContext {

    public static UserEntity getUser() {
        UserPrinciple principle = (UserPrinciple) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        UserEntity user = new UserEntity();
        user.setEmail(principle.getEmail());
        user.setId(principle.getId());
        user.setName(principle.getName());
        user.setEnabled(principle.isEnabled());
        return user;
    }
}
