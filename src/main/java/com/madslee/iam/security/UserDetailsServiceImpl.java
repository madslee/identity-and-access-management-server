package com.madslee.iam.security;

import com.madslee.iam.userManagement.entities.UserEntity;
import com.madslee.iam.userManagement.repositories.UserEntityRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {

    @Autowired
    UserEntityRepo userEntityRepo;

    @Override
    @Transactional
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Optional<UserEntity> user = userEntityRepo.findByEmail(username);
        if (user.isEmpty()) throw new UsernameNotFoundException("User Not Found with -> username or email : " + username);
        else return UserPrinciple.build(user.get());
    }
}
