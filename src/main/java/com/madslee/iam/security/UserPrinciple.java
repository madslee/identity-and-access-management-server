package com.madslee.iam.security;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.madslee.iam.userManagement.entities.UserEntity;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;

public class UserPrinciple implements UserDetails {

    private static final long serialVersionUID = 1L;
    private Long id;
    private String name;
    private String email;
    private boolean isEnabled;

    @JsonIgnore
    private String password;

    public UserPrinciple(Long id, String name, String email, String password, boolean isEnabled) {
        this.id = id;
        this.name = name;
        this.email = email;
        this.password = password;
        this.isEnabled = isEnabled;
    }

    public static UserPrinciple build(UserEntity user) {
        return new UserPrinciple(user.getId(), user.getName(), user.getEmail(), user.getPassword(), user.isEnabled());
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getEmail() {
        return email;
    }

    @Override
    public String getPassword() {
        return password;
    }

    @Override
    public String getUsername() {
        return email;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return null;
    }

    @Override
    public boolean isAccountNonExpired() {
        return isEnabled;
    }

    @Override
    public boolean isAccountNonLocked() {
        return isEnabled;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return isEnabled;
    }

    @Override
    public boolean isEnabled() {
        return isEnabled;
    }

    @Override
    public boolean equals(Object o) {
        if (o instanceof UserPrinciple) {
            return ((UserPrinciple) o).getId().equals(id);
        }
        return false;
    }
}
