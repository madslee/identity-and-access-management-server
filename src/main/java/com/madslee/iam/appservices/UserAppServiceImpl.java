package com.madslee.iam.appservices;

import com.madslee.iam.userManagement.exceptions.AuthenticationException;
import com.madslee.iam.appcommons.email.EmailService;
import com.madslee.iam.userManagement.exceptions.UserManagementExceptionCause;
import com.madslee.iam.userManagement.UserService;
import com.madslee.iam.userManagement.entities.User;
import com.madslee.iam.userManagement.entities.UserEntity;
import com.madslee.iam.security.jwt.JwtProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class UserAppServiceImpl implements UserAppService {

    private UserService userService;
    private EmailService emailService;
    private JwtProvider jwtProvider;

    @Autowired
    public UserAppServiceImpl(UserService userService, EmailService emailService, JwtProvider jwtProvider) {
        this.userService = userService;
        this.emailService = emailService;
        this.jwtProvider = jwtProvider;
    }

    @Override
    @Transactional
    public User saveNewUser(UserEntity userEntity) {
        var user = userService.saveNewUser(userEntity);
        emailService.sendConfirmationEmail(user, user.getConfirmationKeyEntity().getKey());
        return user;
    }

    @Override
    public void confirmAccount(Long userId, String token) {
        userService.confirmUserAccount(userId, token);
    }

    @Override
    public String login(String email, String password) {
        if (userService.isActiveUserAccountAndCorrectCredentials(email, password)) {
            UserEntity user = userService.getUserByEmailUnsafe(email);
            return jwtProvider.getJwt(user);
        }
        throw new AuthenticationException(UserManagementExceptionCause.USER_AUTHENTICATION_FAILED);
    }

    @Override
    public User getUserByJwt(String jwt) {
        String email = jwtProvider.getEmailFromJwt(jwt);
        return userService.getUserByEmailUnsafe(email);
    }
}
