package com.madslee.iam.appservices;

import com.madslee.iam.userManagement.entities.User;
import com.madslee.iam.userManagement.entities.UserEntity;

public interface UserAppService {

    User saveNewUser(UserEntity userEntity);

    void confirmAccount(Long userId, String token);

    String login(String email, String password);

    User getUserByJwt(String jwt);
}
