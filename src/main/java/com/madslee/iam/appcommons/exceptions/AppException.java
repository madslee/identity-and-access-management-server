package com.madslee.iam.appcommons.exceptions;


public class AppException extends RuntimeException {

    private String code;

    public AppException(String cause, String code) {
        super(cause);
        this.code = code;
    }

    public String getCode() {
        return code;
    }
}
