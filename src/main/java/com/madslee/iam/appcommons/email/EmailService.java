package com.madslee.iam.appcommons.email;


import com.madslee.iam.userManagement.entities.User;

public interface EmailService {

    void sendEmail(String recipientEmail, String subject, String body);
    void sendConfirmationEmail(User user, String token);

}
