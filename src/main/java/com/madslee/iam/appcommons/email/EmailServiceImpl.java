package com.madslee.iam.appcommons.email;

import com.madslee.iam.userManagement.entities.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.MailSendException;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

import java.util.Arrays;

import static com.madslee.iam.AppStrings.getString;

@Service
public class EmailServiceImpl implements EmailService {

    private final JavaMailSender emailSender;

    private final Logger logger = LoggerFactory.getLogger(EmailServiceImpl.class);

    @Value("${mail.mock.sending}")
    private boolean mockSending;

    @Value("${spring.mail.username}")
    private String sender;

    public EmailServiceImpl(JavaMailSender emailSender) {
        this.emailSender = emailSender;
    }

    @Override
    public void sendEmail(String recipientEmail, String subject, String body) throws MailSendException {
        var message = new SimpleMailMessage();
        message.setFrom(sender);
        message.setTo(recipientEmail);
        message.setSubject(subject);
        message.setText(body);
        sendEmail(message);
    }

    @Override
    public void sendConfirmationEmail(User user, String token) {
        String confirmationMessageSubject = getString("mail.confirmation.subject");
        String confirmationMessageBody = getString("mail.confirmation.body.pre");

        var message = new SimpleMailMessage();
        message.setFrom(sender);
        message.setTo(user.getEmail());
        message.setSubject(confirmationMessageSubject);
        message.setText(confirmationMessageBody + token);
        sendEmail(message);
    }

    private void sendEmail(SimpleMailMessage mailMessage) {
        logger.info("Sending mail to " + Arrays.toString(mailMessage.getTo()) + " with subject " + mailMessage.getSubject());

        if (mockSending) {
            logger.info("Mock sending enabled, email not actually sent to recipient");
            logger.info("Mail content: " + mailMessage.getText());
            return;
        }

        try {
            emailSender.send(mailMessage);
        } catch (MailSendException e) {
            logger.error("Failed to send mail to " + Arrays.toString(mailMessage.getTo()));
            throw new MailException(MailExceptionCause.MAIL_SENDER_ERROR);
        }
    }
}
