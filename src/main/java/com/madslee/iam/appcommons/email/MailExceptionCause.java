package com.madslee.iam.appcommons.email;

import static com.madslee.iam.AppStrings.getString;

public enum MailExceptionCause {

    MAIL_SENDER_ERROR;
    private final String ERROR_CODE_PREFIX = "MAIL_ERROR_";

    public String getErrorCode() {
        for (int i = 0; i < values().length; i++) {
            if (this == values()[i]) {
                return ERROR_CODE_PREFIX + i + 1;
            }
        }
        return "";
    }

    public String getCause() {
        if (this == MAIL_SENDER_ERROR) return getString("mail.sender.error");
        return "";
    }
}
