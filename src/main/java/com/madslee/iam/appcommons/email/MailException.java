package com.madslee.iam.appcommons.email;

import com.madslee.iam.appcommons.exceptions.AppException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class MailException extends AppException {

    public MailException(MailExceptionCause cause) {
        super(cause.getCause(), cause.getErrorCode());
    }
}
