package com.madslee.iam.controllers;

import com.madslee.iam.appservices.UserAppService;
import com.madslee.iam.controllers.dtos.LoginForm;
import com.madslee.iam.security.SecurityContext;
import com.madslee.iam.userManagement.entities.User;
import com.madslee.iam.userManagement.entities.UserEntity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/user")
public class UserController {

    private UserAppService userAppService;
    private final Logger logger = LoggerFactory.getLogger(UserController.class);

    @Autowired
    public UserController(UserAppService userAppService) {
        this.userAppService = userAppService;
    }

    @PostMapping(consumes = "application/json", produces = "application/json")
    public User saveUser(@RequestBody UserEntity userEntity) {
        return userAppService.saveNewUser(userEntity);
    }

    @PutMapping(value = "/confirm/{userId}/{token}", produces = "application/json")
    public void confirmAccount(@PathVariable Long userId, @PathVariable String token) {
        userAppService.confirmAccount(userId, token);
    }

    @PostMapping(value = "/login", consumes = "application/json", produces = "application/json")
    public String login(@RequestBody LoginForm loginForm) {
        String jwt = userAppService.login(loginForm.getEmail(), loginForm.getPassword());
        return jwt;
    }

    @GetMapping(value = "/me", produces = "application/json")
    public User getUser() {
        return SecurityContext.getUser();
    }
}

