package com.madslee.iam;

import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;

import java.util.Locale;

public class AppStrings {

    private MessageSource messageSource;
    private final Locale locale;

    private AppStrings() {
        this.messageSource = messageSource();
        this.locale = Locale.ENGLISH;
    }

    public static String getString(String key) {
        AppStrings strings = new AppStrings();
        return strings.getMessage(key);
    }

    private String getMessage(String key) {
        return messageSource.getMessage(key, null, locale);
    }

    @Bean
    public MessageSource messageSource() {
        ReloadableResourceBundleMessageSource messageSource = new ReloadableResourceBundleMessageSource();
        messageSource.setBasename("classpath:strings");
        messageSource.setDefaultEncoding("UTF-8");
        return messageSource;
    }
}
