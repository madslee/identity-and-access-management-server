FROM adoptopenjdk/openjdk11:alpine-nightly

RUN addgroup -S app \
 && adduser -D -h /app -G app -S app

USER app
WORKDIR /app

ENV CONTEXT_PATH=/iam \
    HEAP_PERCENTAGE=50.0

COPY build/libs/*.jar iam.jar

CMD export HOST_DOCKER_INTERNAL=$(ip -4 route list match 0/0 | awk '{print $3}') \
 && java \
  -server \
  -XX:InitialRAMPercentage=$HEAP_PERCENTAGE \
  -XX:MaxRAMPercentage=$HEAP_PERCENTAGE \
  -Duser.language=em \
  -Duser.region=EN \
  -Djava.awt.headless=true \
  -agentlib:jdwp=transport=dt_socket,server=y,suspend=n,address=8888 \
  -Dcom.sun.management.jmxremote \
  -Dcom.sun.management.jmxremote.port=9999 \
  -Dcom.sun.management.jmxremote.local.only=false \
  -Dcom.sun.management.jmxremote.authenticate=false \
  -Dcom.sun.management.jmxremote.ssl=false \
  -Djava.io.tmpdir=/tmp \
  -jar iam.jar \
  --server.port=8080 \
  --server.servlet.context-path=$CONTEXT_PATH

EXPOSE 8080 8888 9999
